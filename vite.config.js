import { ViteEjsPlugin } from "vite-plugin-ejs";
import { defineConfig } from "vite";
import * as dotenv from 'dotenv'

export default defineConfig(() => {
  dotenv.config();
  return {
    base: './',
    plugins: [
      // Without Data
      ViteEjsPlugin({
        backendUrl: process.env.VITE_BACKEND_URL,
      }),
    ]
  };
});